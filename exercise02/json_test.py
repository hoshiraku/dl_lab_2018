#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 20:50:00 2018

@author: hoshi
"""

import json
import os
import matplotlib.pyplot as plt

path = os.path.join(os.getcwd(), "results")
RESULT_SETS = 10
learning_curve = []

for run_id in range(4):
    """
    0:
        "lr": 0.001, "num_filters": 32, "filter_size": 3, "batch_size": 128
    1:
        "lr": 0.0001, "num_filters": 32, "filter_size": 3, "batch_size": 128
    2:
        "lr": 0.01, "num_filters": 32, "filter_size": 3, "batch_size": 128
    3:
        "lr": 0.1, "num_filters": 32, "filter_size": 3, "batch_size": 128
    
    """
    
    fname = os.path.join(path, "results_run_%d.json" % run_id)
    fh = open(fname, "r")
    data = json.load(fh)
    learning_curve.append(data['learning_curve'])
    fh.close()

plt.plot(learning_curve[0]) # plotting by columns
plt.plot(learning_curve[1])
plt.plot(learning_curve[2])
plt.plot(learning_curve[3])
plt.legend(['lr = 0.001', 'lr = 0.0001', 'lr = 0.01', 'lr = 0.1'], loc='upper left')
plt.xlabel('epoch')
plt.ylabel('learning curve')
plt.show()

for run_id in range(4, 7):
    print(run_id)
    """
    4:
        "lr": 0.01, "num_filters": 32, "filter_size": 1, "batch_size": 128
    5:
        lr": 0.01, "num_filters": 32, "filter_size": 5, "batch_size": 128
    6:
        lr": 0.01, "num_filters": 32, "filter_size": 7, "batch_size": 128
    2:
        "lr": 0.01, "num_filters": 32, "filter_size": 3, "batch_size": 128
    """
    fname = os.path.join(path, "results_run_%d.json" % run_id)
    fh = open(fname, "r")
    data = json.load(fh)
    learning_curve.append(data['learning_curve'])
    fh.close()

plt.plot(learning_curve[4]) # plotting by columns
plt.plot(learning_curve[5])
plt.plot(learning_curve[6])
plt.plot(learning_curve[2])
plt.legend(['size = 1', 'size = 5', 'size = 7', 'size = 3'], loc='upper left')
plt.xlabel('epoch, lr=0.01')
plt.ylabel('learning curve')
plt.show()

for run_id in range(7, 11):
    print(run_id)
    """
    7:
        "lr": 0.001, "num_filters": 32, "filter_size": 1, "batch_size": 128
    8:
        "lr": 0.001, "num_filters": 32, "filter_size": 3, "batch_size": 128
    9:
        "lr": 0.001, "num_filters": 32, "filter_size": 5, "batch_size": 128
    10:
        "lr": 0.001, "num_filters": 32, "filter_size": 7, "batch_size": 128
    """
    fname = os.path.join(path, "results_run_%d.json" % run_id)
    fh = open(fname, "r")
    data = json.load(fh)
    learning_curve.append(data['learning_curve'])
    fh.close()

plt.plot(learning_curve[7]) # plotting by columns
plt.plot(learning_curve[8])
plt.plot(learning_curve[9])
plt.plot(learning_curve[10])
plt.legend(['size = 1', 'size = 5', 'size = 7', 'size = 3'], loc='upper left')
plt.xlabel('epoch, lr=0.001')
plt.ylabel('learning curve')
plt.show()

