#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 16 20:55:06 2018

@author: hoshi
"""

from __future__ import print_function

import argparse
import gzip
import json
import os
import pickle
import time

import numpy as np
import tensorflow as tf

def one_hot(labels):
    """this creates a one hot encoding from a flat vector:
    i.e. given y = [0,2,1]
     it creates y_one_hot = [[1,0,0], [0,0,1], [0,1,0]]
    """
    classes = np.unique(labels)
    n_classes = classes.size
    one_hot_labels = np.zeros(labels.shape + (n_classes,))
    for c in classes:
        one_hot_labels[labels == c, c] = 1
    return one_hot_labels

# JSON encoder
class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)


def mnist(datasets_dir='./data'):
    if not os.path.exists(datasets_dir):
        os.mkdir(datasets_dir)
    data_file = os.path.join(datasets_dir, 'mnist.pkl.gz')
    if not os.path.exists(data_file):
        print('... downloading MNIST from the web')
        try:
            import urllib
            urllib.urlretrieve('http://google.com')
        except AttributeError:
            import urllib.request as urllib
        url = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
        urllib.urlretrieve(url, data_file)

    print('... loading data')
    # Load the dataset
    f = gzip.open(data_file, 'rb')
    try:
        train_set, valid_set, test_set = pickle.load(f, encoding="latin1")
    except TypeError:
        train_set, valid_set, test_set = pickle.load(f)
    f.close()

    test_x, test_y = test_set
    test_x = test_x.astype('float32')
    test_x = test_x.astype('float32').reshape(test_x.shape[0], 28, 28, 1)
    test_y = test_y.astype('int32')
    valid_x, valid_y = valid_set
    valid_x = valid_x.astype('float32')
    valid_x = valid_x.astype('float32').reshape(valid_x.shape[0], 28, 28, 1)
    valid_y = valid_y.astype('int32')
    train_x, train_y = train_set
    train_x = train_x.astype('float32').reshape(train_x.shape[0], 28, 28, 1)
    train_y = train_y.astype('int32')
    print('... done loading data')
    return train_x, one_hot(train_y), valid_x, one_hot(valid_y), test_x, one_hot(test_y)

# Placaholder variable for the input images
# TODO: Placeholder
x = tf.placeholder(tf.float32, shape=[None, 28*28], name='X')
# Reshape it into [num_images, img_height, img_width, num_channels]
x_image = tf.reshape(x, [-1, 28, 28, 1])
# Placeholder variable for the true labels associated with the images
y_true = tf.placeholder(tf.float32, shape=[None, 10], name='y_true')
# the result from softmax
y_true_cls = tf.argmax(y_true, axis=1)

# Create a Convolution Neural Layer
def new_conv_layer(input, num_input_channels, filter_size, num_filters, name):
        shape = [filter_size, filter_size, num_input_channels, num_filters]
        weights = tf.Variable(tf.truncated_normal(shape, stddev=0.05))
        biases = tf.Variable(tf.constant(0.05, shape=[num_filters]))
        layer = tf.nn.conv2d(input=input, filter=weights, strides=[1, 1, 1, 1], padding='SAME')
        layer += biases
        return layer, weights

# Create a Pooling Layer
def new_pool_layer(input, name):
        layer = tf.nn.max_pool(value=input, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        return layer

# Create a new ReLU Layer
def new_relu_layer(input, name):
        layer = tf.nn.relu(input)
        return layer

# Create a new Fully Connected Layer
def new_fc_layer(input, num_inputs, num_outputs, name):
        shape = [num_inputs, num_outputs]
        weights = tf.Variable(tf.truncated_normal(shape, stddev=0.05))
        biases = tf.Variable(tf.constant(0.05, shape=[num_outputs]))
        layer = tf.matmul(input, weights) + biases
        return layer

# Initialize the FileWriter
#writer = tf.summary.FileWriter("Training_FileWriter/")
#writer1 = tf.summary.FileWriter("Validation_FileWriter/")

# Add the cost and accuracy to summary
#tf.summary.scalar('loss', cost)
#tf.summary.scalar('accuracy', accuracy)

# Merge all summaries together
merged_summary = tf.summary.merge_all()


def train_and_validate(x_train, y_train, x_valid, y_valid, num_epochs, lr, filter_size, num_filters, batch_size):
	# Create convolutional Neural Network
	# convolutional layer 1
    layer_conv1, weights_conv1 = new_conv_layer(input=x_image, num_input_channels=1, filter_size=filter_size, num_filters=num_filters, name="conv1")
	# pooling layer 1
    layer_pool1 = new_pool_layer(layer_conv1, name="pool1")
	# ReLU Layer 1
    layer_relu1 = new_relu_layer(layer_pool1, name="relu1")
	# convolutional layer 2
    layer_conv2, weights_conv2 = new_conv_layer(input=layer_relu1, num_input_channels=num_filters, filter_size=filter_size, num_filters=num_filters, name="conv2")
	# Pooling Layer 2
    layer_pool2 = new_pool_layer(layer_conv2, name="pool2")
	# ReLU Layer2
    layer_relu2 = new_relu_layer(layer_pool2, name="relu2")


	# Flatten Layer
    num_features = layer_relu2.get_shape()[1:4].num_elements()

    layer_flat = tf.reshape(layer_relu2, [-1, num_features])

	# Fully-Connected Layer 1
	# Fully-Connected Layer 1
    layer_fc1 = new_fc_layer(layer_flat, num_inputs=num_features, num_outputs=128, name="fc1")

	# RelU layer 3
    layer_relu3 = new_relu_layer(layer_fc1, name="relu3")

	# Fully-Connected Layer 2
    layer_fc2 = new_fc_layer(input=layer_relu3, num_inputs=128, num_outputs=10, name="fc2")

	# Softmax normalize the output
#	with tf.variable_scope("Softmax"):
    y_pred = tf.nn.softmax(layer_fc2)
    y_pred_cls = tf.argmax(y_pred, axis=1)

	# Cost Function
#	with tf.name_scope("cross_ent"):
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=layer_fc2, labels=y_true)
    cost = tf.reduce_mean(cross_entropy)

	# Use Adam Optimizer
#	with tf.name_scope("optimizer"):
    optimizer = tf.train.AdamOptimizer(learning_rate=lr).minimize(cost)

	# Accuracy
#	with tf.name_scope("accuracy"):
    correct_prediction = tf.equal(y_pred_cls, y_true_cls)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    # TODO: train and validate your convolutional neural networks with the provided data and hyperparameters
    # with tf.Session() as sess:
    # saver = tf.train.Saver()
    saver=tf.train.Saver(max_to_keep=1)
    # save the model of the most accurary
    max_acc = 0
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    
    model = None # empty model
    learning_curve = [] # empty list
    #start_time = time.time()
    for epoch in range(num_epochs):
        
        #train_accuracy = 0
    
        for batch_id in range(0, int(len(x_train)//batch_size)):
            x_train_batch = x_train[batch_id * batch_size: (batch_id + 1) * batch_size]
            y_train_batch = y_train[batch_id * batch_size: (batch_id + 1) * batch_size]
    
            feed_dict_train_batch = {x_image: x_train_batch, y_true: y_train_batch}
    
            sess.run(optimizer, feed_dict=feed_dict_train_batch)
        
        
        feed_dict_valid = {x_image: x_valid, y_true: y_valid}
        val_acc = sess.run(accuracy, feed_dict=feed_dict_valid)
        print('epoch:%d, validation accuracy:%f'%(epoch,val_acc))
        #save model after each epoch
        if val_acc>max_acc:
            max_acc=val_acc
            saver.save(sess,'ckpt/mnist.ckpt',global_step=epoch+1)
            #print('Trained model of epoch %d saved!' %(epoch))
        learning_curve.append(val_acc)
    print('Trained model of epoch saved!')
    sess.close()
    
    print(learning_curve)
    
    model = saver
    
    return learning_curve, model  # TODO: Return the validation error after each epoch (i.e learning curve) and your model

# include train and test together
    
def train_and_validate_v2(x_train, y_train, x_valid, y_valid, x_test, y_test, num_epochs, lr, filter_size, num_filters, batch_size):
	# Create convolutional Neural Network
	# convolutional layer 1
    layer_conv1, weights_conv1 = new_conv_layer(input=x_image, num_input_channels=1, filter_size=3, num_filters=16, name="conv1")
	# pooling layer 1
    layer_pool1 = new_pool_layer(layer_conv1, name="pool1")
	# ReLU Layer 1
    layer_relu1 = new_relu_layer(layer_pool1, name="relu1")
	# convolutional layer 2
    layer_conv2, weights_conv2 = new_conv_layer(input=layer_relu1, num_input_channels=16, filter_size=3, num_filters=16, name="conv2")
	# Pooling Layer 2
    layer_pool2 = new_pool_layer(layer_conv2, name="pool2")
	# ReLU Layer2
    layer_relu2 = new_relu_layer(layer_pool2, name="relu2")


	# Flatten Layer
    num_features = layer_relu2.get_shape()[1:4].num_elements()

    layer_flat = tf.reshape(layer_relu2, [-1, num_features])

	# Fully-Connected Layer 1
	# Fully-Connected Layer 1
    layer_fc1 = new_fc_layer(layer_flat, num_inputs=num_features, num_outputs=128, name="fc1")

	# RelU layer 3
    layer_relu3 = new_relu_layer(layer_fc1, name="relu3")

	# Fully-Connected Layer 2
    layer_fc2 = new_fc_layer(input=layer_relu3, num_inputs=128, num_outputs=10, name="fc2")

	# Softmax normalize the output
#	with tf.variable_scope("Softmax"):
    y_pred = tf.nn.softmax(layer_fc2)
    y_pred_cls = tf.argmax(y_pred, axis=1)

	# Cost Function
#	with tf.name_scope("cross_ent"):
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=layer_fc2, labels=y_true)
    cost = tf.reduce_mean(cross_entropy)

	# Use Adam Optimizer
#	with tf.name_scope("optimizer"):
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=lr).minimize(cost)

	# Accuracy
#	with tf.name_scope("accuracy"):
    correct_prediction = tf.equal(y_pred_cls, y_true_cls)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    # TODO: train and validate your convolutional neural networks with the provided data and hyperparameters
 #   with tf.Session() as sess:
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    
    model = None # empty model
    learning_curve = [] # empty list
    #start_time = time.time()
    for epoch in range(num_epochs):
        
        #train_accuracy = 0
    
        for batch_id in range(0, int(len(x_train)//batch_size)):
            x_train_batch = x_train[batch_id * batch_size: (batch_id + 1) * batch_size]
            y_train_batch = y_train[batch_id * batch_size: (batch_id + 1) * batch_size]
    
            feed_dict_train_batch = {x_image: x_train_batch, y_true: y_train_batch}
    
            sess.run(optimizer, feed_dict=feed_dict_train_batch)
        
        
        feed_dict_valid = {x_image: x_valid, y_true: y_valid}
        result = sess.run(accuracy, feed_dict=feed_dict_valid)
        
        print('epoch:%d, validation loss:%f'%(epoch, result))
        #print(result)
        
        #save model after each epoch
        
        learning_curve.append(result)
    
    print(learning_curve)
    
    feed_dict_test = {x_image: x_test, y_true: y_test}
    test_error = sess.run(accuracy, feed_dict=feed_dict_test)
    print('test error:%f'%(test_error))
    
    return learning_curve, model, test_error 


# test with model parameters
def test(x_test, y_test, model):
    # TODO: test your network here by evaluating it on the test data
	# Create convolutional Neural Network
	# convolutional layer 1
    layer_conv1, weights_conv1 = new_conv_layer(input=x_image, num_input_channels=1, filter_size=3, num_filters=16, name="conv1")
	# pooling layer 1
    layer_pool1 = new_pool_layer(layer_conv1, name="pool1")
	# ReLU Layer 1
    layer_relu1 = new_relu_layer(layer_pool1, name="relu1")
	# convolutional layer 2
    layer_conv2, weights_conv2 = new_conv_layer(input=layer_relu1, num_input_channels=16, filter_size=3, num_filters=16, name="conv2")
	# Pooling Layer 2
    layer_pool2 = new_pool_layer(layer_conv2, name="pool2")
	# ReLU Layer2
    layer_relu2 = new_relu_layer(layer_pool2, name="relu2")


	# Flatten Layer
    num_features = layer_relu2.get_shape()[1:4].num_elements()

    layer_flat = tf.reshape(layer_relu2, [-1, num_features])

	# Fully-Connected Layer 1
	# Fully-Connected Layer 1
    layer_fc1 = new_fc_layer(layer_flat, num_inputs=num_features, num_outputs=128, name="fc1")

	# RelU layer 3
    layer_relu3 = new_relu_layer(layer_fc1, name="relu3")

	# Fully-Connected Layer 2
    layer_fc2 = new_fc_layer(input=layer_relu3, num_inputs=128, num_outputs=10, name="fc2")

	# Softmax normalize the output
#	with tf.variable_scope("Softmax"):
    y_pred = tf.nn.softmax(layer_fc2)
    y_pred_cls = tf.argmax(y_pred, axis=1)
    
	# Cost Function
#	with tf.name_scope("cross_ent"):
#    cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=layer_fc2, labels=y_true)
#    cost = tf.reduce_mean(cross_entropy)

	# Use Adam Optimizer
#	with tf.name_scope("optimizer"):
#    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cost)

	# Accuracy
#	with tf.name_scope("accuracy"):
    correct_prediction = tf.equal(y_pred_cls, y_true_cls)
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    # TODO: train and validate your convolutional neural networks with the provided data and hyperparameters
    # with tf.Session() as sess:
    # saver = tf.train.Saver()
    # save the model of the most accurary
    saver = model
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    
    model_file=tf.train.latest_checkpoint('ckpt/')
    saver.restore(sess,model_file)
    print('Trained model for test restored!')
    feed_dict_test = {x_image: x_test, y_true: y_test}
    test_error = sess.run(accuracy, feed_dict=feed_dict_test)
#    print(test_error)
    print('test error:%f'%(test_error))
    
    sess.close()
    return test_error  # TODO: Return the validation error after each epoch (i.e learning curve) and your model


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--output_path", default="./", type=str, nargs="?",
                        help="Path where the results will be stored")
    parser.add_argument("--input_path", default="./", type=str, nargs="?",
                        help="Path where the data is located. If the data is not available it will be downloaded first")
    parser.add_argument("--learning_rate", default=1e-3, type=float, nargs="?", help="Learning rate for SGD")
    parser.add_argument("--num_filters", default=32, type=int, nargs="?",
                        help="The number of filters for each convolution layer")
    parser.add_argument("--filter_size", default=3, type=int, nargs="?",
                        help="The size of filter in each convolution layer")
    parser.add_argument("--batch_size", default=128, type=int, nargs="?", help="Batch size for SGD")
    parser.add_argument("--epochs", default=30, type=int, nargs="?",
                        help="Determines how many epochs the network will be trained")
    parser.add_argument("--run_id", default=0, type=int, nargs="?",
                        help="Helps to identify different runs of an experiments")

    args = parser.parse_args()
    
    #print("args input:")
    
    # hyperparameters
    lr = args.learning_rate
    num_filters = args.num_filters
    filter_size = args.filter_size
    batch_size = args.batch_size
    epochs = args.epochs

    #print("learning rate :" + str(lr))

    # train and test convolutional neural network
    x_train, y_train, x_valid, y_valid, x_test, y_test = mnist(args.input_path)

    #learning_curve, model = train_and_validate(x_train, y_train, x_valid, y_valid, epochs, lr, filter_size, num_filters, batch_size)
    
    learning_curve, model, test_error = train_and_validate_v2(x_train, y_train, x_valid, y_valid, x_test, y_test, epochs, lr, filter_size, num_filters, batch_size)

    #test_error = test(x_test, y_test, model)
    # save results in a dictionary and write them into a .json file
    results = dict()
    results["lr"] = lr
    results["num_filters"] = num_filters
    results["filter_size"] = filter_size
    results["batch_size"] = batch_size
    results["learning_curve"] = learning_curve
    results["test_error"] = test_error

    path = os.path.join(args.output_path, "results")
    os.makedirs(path, exist_ok=True)

    fname = os.path.join(path, "results_run_%d.json" % args.run_id)
    
    print(results)

    fh = open(fname, "w")
    json.dump(results, fh, cls=MyEncoder)
    fh.close()
